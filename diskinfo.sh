#!/usr/bin/env bash

# Get list of block devices
devices=($(find /dev -maxdepth 1 -exec basename {} \; | grep -Ew 'da[0-9]+' | sort -V | tr '\n' ' '))
diskfile=diskinfo.json
pools=($(zpool list | sed 1,1d | awk '{print $1}' | tr '\n' ' '))
json_header=("DISK","SERIAL","MODEL","SIZE","POOL")
json_fields=(.device, .sn, .model, .size, .pool)

size () {
    local -a units
    local -i scale
    if [[ "$1" == "-si" ]]
    then
        scale=1024
        units=(B KiB MiB GiB TiB EiB PiB YiB ZiB)
        shift
    else
        scale=1000
        units=(B KB MB GB TB EB PB YB ZB)
    fi
    local -i unit=0
    if [ -z "${units[0]}" ]
    then
        unit=1
    fi
    local -i whole=${1:-0}
    local -i remainder=0
    while (( whole >= $scale ))
    do
        remainder=$(( whole % scale ))
        whole=$((whole / scale))
        unit=$(( $unit + 1 ))
    done
    local decimal
    if [ $remainder -gt 0 ]
    then
        local -i fraction="$(( (remainder * 10 / scale)))"
        if [ "$fraction" -gt 0 ]
        then
            decimal=".$fraction"
        fi
    fi
    echo "${whole}${decimal}${units[$unit]}"
}

refresh_json() {
    # Initialize JSON array
    echo "[ " > $diskfile

    # Loop through each device
    for device in ${devices[@]}; do
        # Get device attributes using udevadm
        sn=$(geom disk list $device | awk -F ': ' '/ident:/ {print $2}')
        model=$(geom disk list $device | awk -F ': ' '/descr:/ {print $2}')
        size=$(size $(geom disk list $device | awk -F ': ' '/Mediasize:/ {print $2}' | awk '{print $1}'))
        if [[ $(gpart list | grep -Ew "Name: da[0-9]+" | sort -V | awk '{print $3}' | grep -cw $device) -ne 0 ]]; then
            uuid=$(gpart list $device | grep rawuuid | awk '{print $2}' | tail -n 1)
        else
            uuid='n/a'
        fi
	# Get multipath membership
	for multi in $(gmultipath list | awk -F ": " '/Geom name:/ {print $2}' | sort -V); do
	    if [[ $(gmultipath list $multi | grep -cw $device) -ne 0 ]]; then
	    	multipath=$multi
		break
	    else
	        multipath='-'
	    fi
	done

	# Get zpool membership
        for zpool in "${pools[@]}"; do
            if [[ $(zpool status "$zpool" | grep -cwE "${device}p[0-9]") -ne 0 ]]; then
                pool=$zpool
                break
            elif [[ $(zpool status "$zpool" | grep -c $uuid) -ne 0 ]]; then
                pool=$zpool
                break
	    elif [[ $(zpool status "$zpool" | grep -c "$sn") -ne 0 ]]; then
	    	pool="$zpool"
		break
            else
                pool='-'
            fi
        done

    #echo $device is a member of $pool


        # Build JSON object for device
        object="{ \"device\": \"$device\", \"model\": \"$model\",\"sn\": \"$sn\", \"size\": \"$size\", \"pool\": \"$pool\",\"uuid\": \"$uuid\",\"multipath\": \"$multipath\" }, "

        # Append object to JSON array
        echo $object >> $diskfile
        #json="$json $object"
    done

    # Remove trailing comma and append closing bracket to JSON array
    sed -i '' '$s/,$//' $diskfile
    echo "]" >> $diskfile
}

while getopts 'rhu' OPTION; do
    case "$OPTION" in
        r)
            refresh_json
            ;;
        h)
            echo "By default the script uses the already-generated diskinfo.json file.
	    To regenerate that file, supply the -r flag.
	    To include partition UUIDs with the output, include the -u flag."
            exit 1
            ;;
	u) # Include uuid column
	    json_output='(["DISK","SERIAL","MODEL","SIZE","POOL","UUID"] | (., map(length*"-"))), (.[] | [.device, .sn, .model, .size, .pool, .uuid]) | @csv' $diskfile | column -t -s, | tr -d '"'
	    ;;
        ?)
            echo "I didn't recognize that option. Use -h for the help menu."
            exit 1
            ;;
    esac
done

if [[ ! -f $diskfile ]]; then
	echo "Can't find $diskfile, run the command again with the -r flag."
else
	# Output JSON using jq

	jq -r '(["DISK","SERIAL","MODEL","SIZE","POOL","MULTIPATH"] | (., map(length*"-"))), (.[] | [.device, .sn, .model, .size, .pool, .multipath]) | @csv' $diskfile | column -t -s, | tr -d '"'
fi
