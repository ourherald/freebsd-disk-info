# FreeBSD Disk Info

This script intends to provide a useful interface for reviewing information about connected disks.

It creates a JSON file of data about disks and displays it using `jq`.
